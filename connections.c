/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   connections.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 18:14:33 by vgula             #+#    #+#             */
/*   Updated: 2019/02/01 16:22:23 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	check_connections(t_counter ct, char *input)
{
	unsigned char	connects;

	connects = 0;
	if (ct.y == 0)
		check_connects_top(ct, input, &connects);
	else if (ct.y == 1 || ct.y == 2)
		check_cnncts_middle(ct, input, &connects);
	else
		check_cnncts_bottom(ct, input, &connects);
	return (connects);
}

void	check_connects_top(t_counter ct, char *input, unsigned char *connects)
{
	if (ct.x == 0)
	{
		input[ct.y * 5 + (ct.x + 1)] == '#' ? (*connects)++ : *connects;
		input[(ct.y + 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
	}
	else if (ct.x == 1 || ct.x == 2)
	{
		input[ct.y * 5 + (ct.x - 1)] == '#' ? (*connects)++ : *connects;
		input[(ct.y + 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
		input[ct.y * 5 + (ct.x + 1)] == '#' ? (*connects)++ : *connects;
	}
	else
	{
		input[ct.y * 5 + (ct.x - 1)] == '#' ? (*connects)++ : *connects;
		input[(ct.y + 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
	}
}

void	check_cnncts_middle(t_counter ct, char *input, unsigned char *connects)
{
	if (ct.x == 0)
	{
		input[(ct.y - 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
		input[ct.y * 5 + (ct.x + 1)] == '#' ? (*connects)++ : *connects;
		input[(ct.y + 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
	}
	else if (ct.x == 1 || ct.x == 2)
	{
		input[(ct.y - 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
		input[ct.y * 5 + (ct.x + 1)] == '#' ? (*connects)++ : *connects;
		input[(ct.y + 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
		input[ct.y * 5 + (ct.x - 1)] == '#' ? (*connects)++ : *connects;
	}
	else
	{
		input[(ct.y - 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
		input[ct.y * 5 + (ct.x - 1)] == '#' ? (*connects)++ : *connects;
		input[(ct.y + 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
	}
}

void	check_cnncts_bottom(t_counter ct, char *input, unsigned char *connects)
{
	if (ct.x == 0)
	{
		input[(ct.y - 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
		input[ct.y * 5 + (ct.x + 1)] == '#' ? (*connects)++ : *connects;
	}
	else if (ct.x == 1 || ct.x == 2)
	{
		input[ct.y * 5 + (ct.x - 1)] == '#' ? (*connects)++ : *connects;
		input[(ct.y - 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
		input[ct.y * 5 + (ct.x + 1)] == '#' ? (*connects)++ : *connects;
	}
	else
	{
		input[ct.y * 5 + (ct.x - 1)] == '#' ? (*connects)++ : *connects;
		input[(ct.y - 1) * 5 + ct.x] == '#' ? (*connects)++ : *connects;
	}
}
