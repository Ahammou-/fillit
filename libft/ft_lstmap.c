/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 00:45:44 by vgula             #+#    #+#             */
/*   Updated: 2018/12/01 00:00:17 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*tmp;
	t_list	*new_lst;

	new_lst = NULL;
	if (lst && f)
	{
		if (!(new_lst = ft_lstnew(f(lst)->content, f(lst)->content_size)))
			return (NULL);
		lst = lst->next;
		tmp = new_lst;
		while (lst)
		{
			if (!(tmp->next = ft_lstnew(f(lst)->content, f(lst)->content_size)))
			{
				ft_lstdel(&new_lst, &ft_del);
				return (NULL);
			}
			tmp = tmp->next;
			lst = lst->next;
		}
	}
	return (new_lst);
}
