/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 23:51:59 by vgula             #+#    #+#             */
/*   Updated: 2018/11/30 23:59:16 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *tmp;

	tmp = *alst;
	if (alst && *alst && del)
	{
		while (tmp)
		{
			del(tmp->content, tmp->content_size);
			free(tmp);
			tmp = tmp->next;
		}
		*alst = NULL;
	}
}
