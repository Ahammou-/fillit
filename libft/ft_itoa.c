/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 01:45:55 by vgula             #+#    #+#             */
/*   Updated: 2018/11/30 23:58:32 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	get_size(int n)
{
	size_t	len;

	len = 0;
	if (n < 0)
	{
		n *= -1;
		len++;
	}
	while (n > 0)
	{
		len++;
		n /= 10;
	}
	return (len);
}

static void		check_n_fill(char *str, size_t i, int n, int *neg)
{
	if (n < 0)
	{
		str[i++] = '-';
		*neg = 1;
		n *= -1;
	}
	while (n > 0)
	{
		str[i++] = '0' + (n % 10);
		n /= 10;
	}
}

static char		*rev_str(char *str, int neg)
{
	size_t	i;
	int		j;
	char	tmp[ft_strlen(str) + 1];

	i = 0;
	j = ft_strlen(str) - 1;
	if (neg)
		tmp[i++] = '-';
	while (j >= neg)
		tmp[i++] = str[j--];
	i = 0;
	while (str[i])
	{
		str[i] = tmp[i];
		i++;
	}
	return (str);
}

char			*ft_itoa(int n)
{
	size_t	i;
	char	*str;
	int		neg;

	i = 0;
	neg = 0;
	if (n == 0)
		return (ft_strdup("0"));
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	str = ft_strnew(get_size(n));
	if (!str)
		return (NULL);
	check_n_fill(str, i, n, &neg);
	str = rev_str(str, neg);
	return (str);
}
