/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_free.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 00:18:48 by vgula             #+#    #+#             */
/*   Updated: 2018/12/04 19:58:35 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strjoin_free(char *s1, char *s2, size_t i)
{
	char	*newstr;

	if (!(newstr = ft_strjoin(s1, s2)))
		return (NULL);
	if (i == 1)
		ft_strdel(&s1);
	else if (i == 2)
		ft_strdel(&s2);
	else if (i == 3)
	{
		ft_strdel(&s1);
		ft_strdel(&s2);
	}
	return (newstr);
}
