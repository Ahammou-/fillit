/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 22:34:09 by vgula             #+#    #+#             */
/*   Updated: 2018/11/30 23:55:58 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *str)
{
	char	sign;
	size_t	nb;

	nb = 0;
	sign = 1;
	while (*str == ' ' || *str == '\f' || *str == '\n' \
			|| *str == '\r' || *str == '\t' || *str == '\v')
		str++;
	if (*str == '-')
		sign = -1;
	if (*str == '+' || *str == '-')
		str++;
	while (ft_isdigit(*str))
	{
		nb *= 10;
		nb += (*str - '0');
		str++;
	}
	if (nb > 9223372036854775807)
		return (sign == 1 ? -1 : 0);
	return ((sign) * (int)nb);
}
