/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 00:07:15 by vgula             #+#    #+#             */
/*   Updated: 2018/12/01 00:11:42 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(char const *s)
{
	size_t	i;
	size_t	j;
	char	*rev;

	i = 0;
	j = ft_strlen(s) - 1;
	if (!(rev = ft_strnew(j + 1)))
		return (NULL);
	while (j > 0)
		rev[i++] = s[j--];
	rev[i] = s[j];
	return (rev);
}
