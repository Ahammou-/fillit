/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 23:51:04 by vgula             #+#    #+#             */
/*   Updated: 2018/12/01 00:12:03 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	char	*newstr;

	if (!s)
		return (NULL);
	i = 0;
	newstr = ft_strnew(len);
	if (!newstr)
		return (NULL);
	newstr = ft_strncpy(newstr, (s + start), len);
	return (newstr);
}
