/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 03:46:46 by vgula             #+#    #+#             */
/*   Updated: 2018/12/01 00:08:08 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	j;

	j = 0;
	i = ft_strlen(dst);
	if (size <= i)
		return (ft_strlen(src) + size);
	while (src[j] && (size - i - 1) > 0)
	{
		dst[i + j] = src[j];
		j++;
		size--;
	}
	dst[i + j] = '\0';
	return (i + ft_strlen(src));
}
