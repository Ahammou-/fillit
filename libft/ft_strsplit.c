/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 13:49:22 by vgula             #+#    #+#             */
/*   Updated: 2018/12/01 00:11:49 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	words_count(char const *s, char c)
{
	size_t	in;
	size_t	nb_words;

	in = 0;
	nb_words = 0;
	while (*s)
	{
		if (in == 1 && *s == c)
			in = 0;
		if (in == 0 && *s != c)
		{
			in = 1;
			nb_words++;
		}
		s++;
	}
	return (nb_words);
}

static size_t	*tab_words_length(char const *s, char c, size_t nb_words)
{
	size_t	i;
	size_t	len;
	size_t	*tab_len;

	i = 0;
	tab_len = (size_t *)malloc(sizeof(size_t) * nb_words);
	if (!tab_len)
		return (NULL);
	while (i < nb_words)
	{
		if (*s != c)
		{
			len = 0;
			while (*s && *s != c)
			{
				len++;
				s++;
			}
			tab_len[i++] = len;
		}
		s++;
	}
	return (tab_len);
}

static char		**nb_words_zero(void)
{
	char	**ret;

	ret = (char **)malloc(sizeof(char *));
	if (!ret)
		return (NULL);
	ret[0] = NULL;
	return (ret);
}

static char		**create_tab_words(char const *s, char c, size_t nb_words)
{
	size_t	i;
	size_t	*words_length;
	char	**tab_words;

	i = 0;
	words_length = tab_words_length(s, c, nb_words);
	tab_words = (char **)malloc(sizeof(char *) * (nb_words + 1));
	if (!words_length || !tab_words)
		return (NULL);
	while (i < nb_words)
	{
		tab_words[i] = ft_strnew(words_length[i]);
		if (!tab_words[i])
		{
			ft_free_tabtab(tab_words);
			ft_memdel((void **)&words_length);
			return (NULL);
		}
		i++;
	}
	tab_words[i] = 0;
	ft_memdel((void **)&words_length);
	return (tab_words);
}

char			**ft_strsplit(char const *s, char c)
{
	size_t	i;
	size_t	j;
	size_t	nb_words;
	char	**tab_words;

	if (!s)
		return (NULL);
	i = 0;
	nb_words = words_count(s, c);
	if (nb_words == 0)
		return (nb_words_zero());
	tab_words = create_tab_words(s, c, nb_words);
	if (!tab_words)
		return (NULL);
	while (i < nb_words)
	{
		while (*s == c)
			s++;
		j = 0;
		while (*s && *s != c)
			tab_words[i][j++] = *s++;
		i++;
	}
	return (tab_words);
}
