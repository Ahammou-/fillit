/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpush_back.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 20:10:41 by vgula             #+#    #+#             */
/*   Updated: 2019/01/18 11:34:14 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstpush_back(t_list **lst, void *content,
													size_t const content_size)
{
	t_list *tmp;

	if (!lst)
		return ;
	tmp = *lst;
	if (!tmp)
	{
		if (!(*lst = ft_lstnew(content, content_size)))
			return ;
	}
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		if (!(tmp->next = ft_lstnew(content, content_size)))
			ft_lstdel(lst, ft_del);
	}
}
