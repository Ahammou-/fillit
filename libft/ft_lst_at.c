/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_at.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 01:05:45 by vgula             #+#    #+#             */
/*   Updated: 2018/11/30 23:58:44 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lst_at(t_list *lst, size_t rang)
{
	size_t i;

	i = 1;
	while (lst->next != NULL && i < rang)
	{
		if (i == rang)
			return (lst);
		lst = lst->next;
		i++;
	}
	return (NULL);
}
