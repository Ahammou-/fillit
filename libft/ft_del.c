/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_del.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 22:30:05 by vgula             #+#    #+#             */
/*   Updated: 2019/01/18 11:34:56 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_del(void *content, size_t content_size)
{
	size_t	i;

	i = content_size / sizeof(void *);
	while (i--)
		free(((void **)content)[i]);
	free(content);
	content = NULL;
	content_size = 0;
}
