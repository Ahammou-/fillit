/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   coordinates.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 18:16:42 by vgula             #+#    #+#             */
/*   Updated: 2019/01/30 21:44:05 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	get_most_left_up_coords(t_ttrcoord *array, t_coord min)
{
	unsigned char	i;

	i = 0;
	while (i < 4)
	{
		array->tab_coord[i].x -= min.x;
		array->tab_coord[i].y -= min.y;
		i++;
	}
}

void	get_coord(t_counter *ct, t_ttrcoord *array)
{
	static t_coord	min;

	if (ct->j == 0)
	{
		min.x = ct->x;
		min.y = ct->y;
	}
	else
	{
		if (ct->x < min.x)
			min.x = ct->x;
		if (ct->y < min.y)
			min.y = ct->y;
	}
	array[ct->tetri].tab_coord[ct->j].x = ct->x;
	array[ct->tetri].tab_coord[ct->j].y = ct->y;
	if (ct->j == 3)
	{
		get_most_left_up_coords(&array[ct->tetri], min);
		array[ct->tetri].id = ct->id;
		ct->j = 0;
		ct->id++;
	}
	else
		ct->j++;
}
