# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vgula <vgula@student.s19.be>               +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/08 22:11:55 by vgula             #+#    #+#              #
#    Updated: 2019/02/01 16:24:11 by vgula            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FLAGS = -Wall -Werror -Wextra
FILES = main_fillit validation connections coordinates algo
SRC = $(addsuffix .c, $(FILES))
OBJ = $(addsuffix .o, $(FILES))
INCL = ./
NAME = fillit

all: $(NAME)

$(NAME):
	@make -C ./libft
	@gcc $(FLAGS) -c $(SRC) -I $(INCL)
	@gcc $(OBJ) ./libft/libft.a -o $(NAME)

clean:
	@make clean -C ./libft
	@/bin/rm -f $(OBJ)

fclean: clean
	@make fclean -C ./libft
	@/bin/rm -f $(NAME)

re: fclean all
