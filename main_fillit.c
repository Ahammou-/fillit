/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_fillit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/10 13:32:41 by vgula             #+#    #+#             */
/*   Updated: 2019/02/01 17:32:22 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_sqrt(int nb)
{
	int i;
	int sqrt;

	i = 1;
	sqrt = 1;
	if (nb <= 0)
		return (0);
	while (sqrt <= nb)
	{
		i++;
		sqrt = i * i;
		if (sqrt == nb)
			return (i);
	}
	return (i);
}

int		file_error(int argc)
{
	if (argc != 2)
		ft_putendl("usage: ./fillit filename");
	else
		ft_putendl("error");
	return (-1);
}

void	display_board(char *board, int size)
{
	int	i;
	int	tmp;

	i = 0;
	tmp = size - 1;
	while (i < size * size)
	{
		ft_putchar(board[i]);
		if (i == tmp)
		{
			ft_putchar('\n');
			tmp += size;
		}
		i++;
	}
}

int		main(int argc, char **argv)
{
	int					fd;
	int					ret;
	int					size;
	char				board[BUFF_SIZE + 1];
	static t_ttrcoord	array[26];

	if (argc != 2 || (fd = open(argv[1], O_RDONLY)) == -1)
		return (file_error(argc));
	ret = read(fd, board, BUFF_SIZE + 1);
	board[ret] = '\0';
	if (ret % 21 != 20 || check_input(board, array) == -1)
		return (file_error(argc));
	ft_memset(board, '.', MAX_MAP_SIZE);
	board[MAX_MAP_SIZE] = '\0';
	ret = (ret + 1) / 21;
	size = ft_sqrt(ret * 4);
	solve_input(board, ret, &size, array);
	display_board(board, size);
	if (close(fd) != 0)
		return (file_error(argc));
	return (0);
}
