/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/10 13:32:35 by vgula             #+#    #+#             */
/*   Updated: 2019/02/01 15:47:02 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	init_counters(t_counter *ct)
{
	ct->i = 0;
	ct->ii = 0;
	ct->j = 0;
	ct->x = 0;
	ct->y = 0;
	ct->id = 'A';
	ct->tetri = 0;
}

void	increment_counters(t_counter *ct)
{
	ct->i++;
	if (ct->y == 4)
	{
		ct->y = 0;
		ct->ii = ct->i;
		ct->tetri++;
	}
	else
		ct->x++;
	if (ct->x == 5)
	{
		ct->x = 0;
		ct->y++;
	}
}

int		check_char(t_counter ct, char c, char *input)
{
	unsigned char			tmp;
	static unsigned char	connects;

	tmp = 0;
	if (ct.x == 0 && ct.y == 0)
		connects = 0;
	if (ct.x < 4 && ct.y < 4 && c != '.' && c != '#')
		return (-1);
	if ((ct.x == 4 || ct.y == 4) && c != '\n')
		return (-1);
	if (c == '#')
	{
		if ((tmp = check_connections(ct, input)) > 3 || tmp == 0)
			return (-1);
		else if (((connects += tmp) > 6 && (connects != 8)))
			return (-1);
	}
	if (ct.x == 3 && ct.y == 3 && connects < 6)
		return (-1);
	return (0);
}

int		check_input(char *input, t_ttrcoord *array)
{
	int			ret;
	t_counter	ct;

	init_counters(&ct);
	while (input[ct.i] != '\0')
	{
		if ((ret = check_char(ct, input[ct.i], &input[ct.ii])) != 0)
			return (ret);
		if (input[ct.i] == '#')
			get_coord(&ct, array);
		increment_counters(&ct);
	}
	return (0);
}
