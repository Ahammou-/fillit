/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 14:37:32 by vgula             #+#    #+#             */
/*   Updated: 2019/02/01 16:18:38 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	check_pos(char *board, unsigned char size, t_ttrcoord *tab, t_coord ct)
{
	unsigned char	i;
	unsigned char	j;
	t_coord			space;

	j = 0;
	space.x = size - ct.x;
	space.y = size - ct.y;
	while (j < 4)
	{
		i = (tab->tab_coord[j].y * size) + tab->tab_coord[j].x;
		if (board[i] != '.')
			return (-1);
		if (tab->tab_coord[j].x >= space.x || tab->tab_coord[j].y >= space.y)
			return (-1);
		j++;
	}
	return (0);
}

void	put_to_board(char *board, t_ttrcoord *tab, unsigned char size)
{
	unsigned char	i;
	unsigned char	j;

	j = 0;
	while (j < 4)
	{
		i = (tab->tab_coord[j].y * size) + tab->tab_coord[j].x;
		board[i] = tab->id;
		j++;
	}
}

void	remove_tetri(char *board, t_ttrcoord *tab, unsigned char size)
{
	unsigned char	i;
	unsigned char	j;

	j = 0;
	while (j < 4)
	{
		i = (tab->tab_coord[j].y * size) + tab->tab_coord[j].x;
		board[i] = '.';
		j++;
	}
}

void	algo(char *board, int *b_size, t_ttrcoord *tab, t_ct *ct)
{
	if (check_pos(&board[ct->i], *b_size, &tab[ct->ii], ct->ct) != -1)
	{
		put_to_board(&board[ct->i], &tab[ct->ii], *b_size);
		ct->j[ct->ii] = ct->i;
		ct->i = 0;
		ct->ii++;
	}
	else if (ct->i == (*b_size) * (*b_size) - 3)
	{
		if (ct->ii == -1)
		{
			ft_memset(board, '.', (*b_size) * (*b_size));
			(*b_size)++;
			ct->i = 0;
			ct->ii = 0;
		}
		else
		{
			remove_tetri(&board[ct->j[ct->ii - 1]], &tab[ct->ii - 1], *b_size);
			ct->i = ct->j[ct->ii - 1] + 1;
			ct->ii--;
		}
	}
	else
		ct->i++;
}

void	solve_input(char *board, char nb_tetri, int *b_size, t_ttrcoord *tab)
{
	t_ct	counter;

	counter.i = 0;
	counter.ii = 0;
	while (counter.i < ((*b_size) * (*b_size)) && counter.ii < nb_tetri)
	{
		counter.ct.x = counter.i % (*b_size);
		counter.ct.y = counter.i / (*b_size);
		algo(board, b_size, tab, &counter);
	}
}
