/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgula <vgula@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/10 13:32:56 by vgula             #+#    #+#             */
/*   Updated: 2019/02/01 16:14:39 by vgula            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include "./libft/libft.h"

# define BUFF_SIZE (21 * 26 - 1)
# define MAX_MAP_SIZE 196

typedef struct	s_coord
{
	unsigned char	x;
	unsigned char	y;
}				t_coord;

typedef struct	s_ttrcoord
{
	t_coord			tab_coord[4];
	unsigned char	id;
}				t_ttrcoord;

typedef struct	s_ct
{
	short	i;
	short	ii;
	short	j[26];
	t_coord	ct;
}				t_ct;

typedef struct	s_counter
{
	unsigned short	i;
	unsigned short	ii;
	unsigned char	j;
	unsigned char	x;
	unsigned char	y;
	unsigned char	id;
	unsigned char	tetri;
}				t_counter;

int				file_error();
int				ft_sqrt(int nb);
int				check_input(char *input, t_ttrcoord *array);
int				check_char(t_counter ct, char c, char *input);
char			check_connections(t_counter ct, char *input);
void			check_connects_top(t_counter ct, char *input,
													unsigned char *connects);
void			check_cnncts_middle(t_counter ct, char *input,
													unsigned char *connects);
void			check_cnncts_bottom(t_counter ct, char *input,
													unsigned char *connects);
void			init_counters(t_counter *ct);
void			increment_counters(t_counter *ct);
void			get_coord(t_counter *ct, t_ttrcoord *array);
void			get_most_left_up_coords(t_ttrcoord *array, t_coord min);
void			solve_input(char *board, char nb_tetri, int *b_size,
															t_ttrcoord *tab);
void			algo(char *board, int *b_size, t_ttrcoord *tab, t_ct *ct);
char			check_pos(char *board, unsigned char size, t_ttrcoord *tab,
																	t_coord ct);
void			put_to_board(char *board, t_ttrcoord *tab,
														unsigned char size);
void			remove_tetri(char *board, t_ttrcoord *tab, unsigned char size);
void			display_board(char *board, int size);
#endif
